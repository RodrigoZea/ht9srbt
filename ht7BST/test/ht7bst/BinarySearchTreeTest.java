/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht7bst;

import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author USER
 */
public class BinarySearchTreeTest {
    
    public BinarySearchTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of insert method, of class BinarySearchTree.
     */
    @Test
    public void testInsert() {
        System.out.println("insert");
        Node newNode = new Node("asd", "asd");
        BinarySearchTree instance = new BinarySearchTree();
        instance.insert(newNode);
        // TODO review the generated test code and remove the default call to fail.
        Assert.assertNotNull(instance);
    }

   
    /**
     * Test of search method, of class BinarySearchTree.
     */
    @Test
    public void testSearch() {
        System.out.println("search");
        
        String dato = "dog";
        BinarySearchTree instance = new BinarySearchTree();
        
        Node newNode = new Node("dog", "perro");
        instance.insert(newNode);
        
        String expResult = "perro";
        String result = instance.search(dato);
        
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
}
  