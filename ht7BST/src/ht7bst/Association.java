
package ht7bst;

import java.util.Map;

/**
 * clase de asociacion
 */

public class Association <K,V> implements Map.Entry<K,V>{
    
    protected K theKey;
    protected V theValue;
    
    public Association(K key, V value){
        theKey = key;
        theValue = value;
    }
    
     public Association(K key)
    {
        this(key,null);
    }

    @Override
    public K getKey() {
        return theKey;
    }

    @Override
    public V getValue() {
        return theValue;
    }

    @Override
    public V setValue(V value) {
        V oldValue = theValue;
        theValue = value;
        return oldValue;
    }
    
}