/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht7bst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 *
 * @author molin
 */
public class Ht7BST {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        BinarySearchTree arbol= new BinarySearchTree<Association<String, String>>();
        ArrayList asociaciones = new ArrayList<Association<String, String>>();
         BufferedReader reader;
        ArrayList<String> palabras = new ArrayList<String>();
        //Escoge archivo
        JFileChooser menu;
        //Filtro para archivo unicamente TXT
        FileNameExtensionFilter txtOnly;
        Scanner scan = new Scanner(System.in);
            
        menu = new JFileChooser();
        txtOnly= new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        menu.setFileFilter(txtOnly);    
        
        int returnV = menu.showOpenDialog(null);
        
        if (returnV == JFileChooser.APPROVE_OPTION){ 
        //---Leer archivo--//        
            try {
                File target=menu.getSelectedFile().getAbsoluteFile();
                reader = new BufferedReader(new FileReader(target));  
            
                String leer = "";
            
                while ((leer = reader.readLine()) != null){
                    palabras.add(leer);
                }
                for(int i=0; i<palabras.size()-1;i++){
                    //se separan las palabras
                    int lugar=palabras.get(i).indexOf(",");
                    String ingles=palabras.get(i).substring(1,lugar);
                    String espanol=palabras.get(i).substring(lugar+1,palabras.get(i).length()-1);
                    //asociaciones agregadas
                   Node nodoPalabra= new Node(ingles,espanol);
                    asociaciones.add(nodoPalabra);
                    arbol.insert(nodoPalabra);
                }
                //recorriendo arbol...
                arbol.inorder();
                
                File x = new File("texto.txt");
                FileReader reader2 = new FileReader(x);
                 BufferedReader bufferedReader = new BufferedReader(reader2);
                String linea = "";
                Scanner scanner = new Scanner(reader2);
                String palabra = "";
        
                while (scanner.hasNextLine()) {
                    linea += scanner.nextLine();
                }
        
        String porTraducir[] = linea.split(" ");

        
        String res="";
        
        String word;
        for(String p: porTraducir){
            word = p.toLowerCase();
            res += arbol.search(word) + " ";
            
        }
                
        System.out.println("----------------------------------");
        System.out.println("Traduccion del documento.");
        System.out.println(res);
        System.out.println("----------------------------------");       
                
                
              } catch (IOException ex) {
                System.out.println("ERROR al leer archivo.");
            }     
                
                
                
         
            }    
                
        else{ 
            System.out.println("Operacion cancelada.");
            System.exit(0);
        }
        

    }
    
}
