package ht7bst;
public class BinarySearchTree<E> 
{
    private Node root;
    
    /**
     * constructor..
     */
    public BinarySearchTree()
    // post: constructor that generates an empty node
    {
        root = null;
    }
    /**
     * determina donde deberia construirse un nodo, dependiendo del root.
     * @param newNode: nodo que se desea 
     * 
     * inspirado de de: https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
     */
    public void insert(Node newNode) 
    {
        root = insertRec(root, newNode);
    }
     
    /**
     * Funcion recursiva para insertar un nuevo nodo en el árbol
     * @param root: conforme se revisa el árbol, root toma el valor del nodo (que ya está en el árbol) que se compara con el 
     * nodo que se quiere insertar.
     * @param nodo: el que se quiere insertar
     * @return Node: referencia a la posición del árbol del nodo insertado.
     * 
     * inspirado de: https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
     */
    public Node insertRec(Node root, Node nodo) 
    {
 
        /* Si el árbol está vacío, establece el nodo argumento como la raíz del árbol */
        if (root == null) 
        {
            root = nodo;
            return root;
        }
        /* si no, revisa donde debe ir el nodo */
        else if ((root.getKey()).compareTo(nodo.getKey()) > 0) //devuelve positivo si el primer string es menor que el segundo
        {
            root.setLeft(insertRec(root.getLeft(), nodo)); 
        }
        else if ((root.getKey().compareTo(nodo.getKey()) < 0))
        {
            root.setRight(insertRec(root.getRight(), nodo));//manda a izq
            root.getRight().setParent(root); //relacion padre hijo
        }
 
        /* Devuelve referencia a nodo */
      return root;
    }
    /**
     * busca si una palabra en inglés existe en el árbol y su traducción.
     * @param dato Palabra a buscar en el nodo.
     * @return Node: nodo que contiene la referencia de nodo.
     * 
     * inspirado de: https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
     */
    public String search(String dato){
        
        if (root == null){
            
            return "*" + dato + "*";
        }else{
            return root.search(dato);
        }
    }
    
    /**
     * funcion recursiva de recorrido
     * inspirado en:https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
     */
    public void inorder()  {
       inorderRec(root);
    }
 
    /**
     * recorre arbol inorder
     * @param root: nodo que actualmente se está comparando
     * inspirado en: https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
     */
    public void inorderRec(Node root) {
        if (root != null) {
            inorderRec(root.getLeft());
            System.out.println(root.toString());
            inorderRec(root.getRight());
        }
    }

    /**
     * obtiene raiz
     * @return Informacion de la raiz
     */
    public Node getRoot() {
        return root;
    }
    
    
}
